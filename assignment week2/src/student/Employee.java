package student;

import java.util.ArrayList;
import java.util.List;

public class Employee {
	private int id;
	private String name;
	private int age;
	private int salary;
	private String department;
	private String city;
	
	public Employee(int id, String name, int age, int salary, String department, String city) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.salary = salary;
		this.department = department;
		this.city = city;
	}
//	@Override
//	public int compareTo(Employee o) {
//		// TODO Auto-generated method stub
//		// if two product id or name or price are equal it return 0 if first > second return +ve or -ve 
//		//return this.pid-o.pid;		
//		//return o.pid -this.pid;			 
//		//return this.pname.compareTo(o.pname); 			
//		return o.name.compareTo(this.name); 				
//		//return (int)(this.price-o.price);						
//		//return (int)(o.price-this.price);								
//	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public Employee() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "\n    " + id + "     " + name + "        " + age + "      " + salary + "      "
				+ department + "      " + city + "\n";
	}
	
		
		
	
	
	}
	
	
	
