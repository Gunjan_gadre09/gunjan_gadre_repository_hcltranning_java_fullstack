create database travel_gunjan;

use travel_gunjan;

create table PASSENGER
 (Passenger_name varchar(20), 
  Category               varchar(20),
   Gender                 varchar(20),
   Boarding_City      varchar(20),
   Destination_City   varchar(20),
  Distance                int,
  Bus_Type             varchar(20)
);


create table PRICE
(
             Bus_Type   varchar(20),
             Distance    int,
              Price      int
          );

insert into passenger values('Sejal','AC','F','Bengaluru','Chennai',350,'Sleeper');
insert into passenger values('Anmol','Non-AC','M','Mumbai','Hyderabad',700,'Sitting');
insert into passenger values('Pallavi','AC','F','panaji','Bengaluru',600,'Sleeper');
insert into passenger values('Khusboo','AC','F','Chennai','Mumbai',1500,'Sleeper');
insert into passenger values('Udit','Non-AC','M','Trivandrum','panaji',1000,'Sleeper');
insert into passenger values('Ankur','AC','M','Nagpur','Hyderabad',500,'Sitting');
insert into passenger values('Hemant','Non-AC','M','panaji','Mumbai',700,'Sleeper');
insert into passenger values('Manish','Non-AC','M','Hyderabad','Bengaluru',500,'Sitting');
insert into passenger values('Piyush','AC','M','Pune','Nagpur',700,'Sitting');

select * from passenger;

insert into price values('Sleeper',350,770);
insert into price values('Sleeper',500,1100);
insert into price values('Sleeper',600,1320);
insert into price values('Sleeper',700,1540);
insert into price values('Sleeper',1000,2200);
insert into price values('Sleeper',1200,2640);
insert into price values('Sleeper',350,434);
insert into price values('Sitting',500,620);
insert into price values('Sitting',500,620);
insert into price values('Sitting',600,744);
insert into price values('Sitting',700,868);
insert into price values('Sitting',1000,1240);
insert into price values('Sitting',1200,1488);
insert into price values('Sitting',1500,1860);

select * from price;

select count(gender) from passenger where distance > 600 and gender="f";
select count(gender) from passenger where distance > 600 and gender="m";


select min(price) from price where bus_type ='sleeper';


select Passenger_name from passenger where passenger_name like 's%';


select p1.passenger_name,p1.Boarding_City,p1.destination_city,p1.bus_type,p2.price from passenger p1 inner join price p2 on p1.distance=p2.distance;


select p1.passenger_name,p2.price from passenger p1,price p2 where p1.bus_type='sitting' and p2.distance=1000;


select p2.price from passenger p1 inner join price p2 on p1.destination_city='bengaluru' and p1.boarding_city='panaji' and p1.passenger_name='pallavi';


select distinct(distance) from passenger order by distance desc;


select passenger_name,distance*100/(select sum(distance) from passenger) as perc_distance from passenger;


create or replace view passenger_view as select passenger_name ,category from  passenger;
select * from passenger_view;
select passenger_name from passenger_view where category='AC';


delimiter !
create procedure findno_passenger(in bus_t varchar(20),out total_pass int)
begin
select count(passenger_name) into total_pass from passenger where bus_type=bus_t;
end!
call findno_passenger('sleeper', @total_pass);
 !
select @total_pass;
!
delimiter ;


select * from passenger limit 5 offset 1;