package com.grtlearning.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.grtlearning.bean.Orders;

@Repository
public interface OrderDao extends JpaRepository<Orders, Integer>{
	@Query("select aa from Orders aa where aa.name=:name")
	List<Orders> getByname (@Param("name")String name);

}
