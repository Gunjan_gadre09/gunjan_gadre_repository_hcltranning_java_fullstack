package com.grtlearning.bean;

import javax.persistence.Entity;
import javax.persistence.Id;
@Entity
public class Foods {
	@Id
	private int id;
	private String dish;
	private int price;
	private String uname;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDish() {
		return dish;
	}
	public void setDish(String dish) {
		this.dish = dish;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	public Foods(int id, String dish, int price, String uname) {
		super();
		this.id = id;
		this.dish = dish;
		this.price = price;
		this.uname = uname;
	}
	public Foods() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Foods [id=" + id + ", dish=" + dish + ", price=" + price + ", uname=" + uname + "]";
	}
	
}
