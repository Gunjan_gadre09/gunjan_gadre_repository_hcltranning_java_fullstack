package com.grtlearning.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Credential {
	@Id
	private String email;
	private String passward;
	private String uname;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassward() {
		return passward;
	}
	public void setPassward(String passward) {
		this.passward = passward;
	}
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	@Override
	public String toString() {
		return "Credential [email=" +email + ", passward=" + passward + ", uname=" + uname + "]";
	}
	
}
