package com.grtlearning.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.grtlearning.bean.Credential;
import com.grtlearning.dao.CdredentialsDao;

@Service
public class CredentialService {
	@Autowired
	CdredentialsDao credentialDao;

	public List<Credential> getCredential(){
		return credentialDao.findAll();
	}

	public int storeCredentialsInfo(Credential cre) {
			credentialDao.save(cre);
			return 1;	
	}

	public Credential userLoginInfo(String email, String passward) {
		Credential ad=credentialDao.existsByEmail(email, passward);
		if(ad==null) {
			return null;
		}else {
			return credentialDao.existsByEmail(email, passward);
		}
	
	}

	public String userDel(String id) {
		if(credentialDao.existsById(null)) {
			credentialDao.deleteById(id);
			return "user deleted Successfully";
		}else {
			return "Uders details for this id is not present";
		}
		
	}

}
