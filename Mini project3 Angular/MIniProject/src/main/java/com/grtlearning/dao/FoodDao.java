package com.grtlearning.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.grtlearning.bean.Foods;
@Repository
public interface FoodDao extends JpaRepository<Foods, Integer>{

}
