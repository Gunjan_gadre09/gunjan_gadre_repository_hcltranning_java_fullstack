package com.grtlearning.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Admin {
	@Id
private String username;
private String passward;

public String getUsername() {
	return username;
}
public void setUsername(String username) {
	this.username = username;
}
public String getPassward() {
	return passward;
}
public void setPassward(String passward) {
	this.passward = passward;
}

@Override
public String toString() {
	return "Admin [email=" + username + ", passward=" + passward + "]";
}

}
