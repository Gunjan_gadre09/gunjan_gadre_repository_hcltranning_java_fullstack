package greatlearning.book.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import greatlearning.book.bean.BooksDetails;
import greatlearning.book.sorting.SortBook;
import greatlearning.book.sorting.sortBookcount;


public class booksServiceClass  {
	List<BooksDetails> listOfBook=new ArrayList();
	public booksServiceClass() {
		//listOfBook.add(new BooksDetails("gunjan", 240, 0, null));
	}
	
	//adding book
	public String addbookdetail(BooksDetails book) {
		Iterator<BooksDetails> li = listOfBook.iterator();
			listOfBook.add(book);
		return "Record stored successfully";
	}
// for showing all books
	public List<BooksDetails> getDetailsOfBooks() {
		return listOfBook;
	}
	
	//for updating records
	public List<BooksDetails> getupdateInRecord(String Nbook,BooksDetails book) {

		Iterator<BooksDetails> li = listOfBook.iterator();
		while(li.hasNext()) {
			BooksDetails listt= li.next();
			if(listt.getBookName().contains(Nbook)) {
				listOfBook.remove(listt);
				listOfBook.add(book);
				break;
			}else {
				System.out.println("no such books in list");
			}

		}
		return listOfBook;
      }
	//for delating records
	public List<BooksDetails> deleteEmployeeInfo(String name) {
		//boolean flag=false;
		Iterator<BooksDetails> li = listOfBook.iterator();
		while(li.hasNext()) {
			BooksDetails emp= li.next();
				if(emp.getBookName().contains(name)) {
					listOfBook.remove(emp);
					break;
				}else {
					System.out.println("no such books in list");
				}
	
		}
		return listOfBook;
		
}
	//for getting totall count
	public int getCount() {
		Iterator<BooksDetails> li = listOfBook.iterator();
		int count=0;
		while(li.hasNext()) {
			BooksDetails emp= li.next();
			count=count+=emp.getNoOfCopies();
		}
		return count;
	}
	//for books under autobiography
	public String getbookUnderAutoG()
	{	
		Iterator<BooksDetails> li = listOfBook.iterator();
		while(li.hasNext()) {
		BooksDetails listt= li.next();
		try {
		if(listt.getGenreOfBooks()==null) {
			throw new IllegalArgumentException("generics of book should not be null");
		}
		else if(listt.getGenreOfBooks().contains("autobiography")) {
				return listt.getBookName();
			}
		}catch(IllegalArgumentException e) {
			System.out.println(e);
		}
		}//}
		return "not any book of this gen";
	}
	
	//sorting book by price low to high
	public List<BooksDetails>sortByPrice(){
		Collections.sort(listOfBook, new SortBook());
		return listOfBook;
	}
	
	// sorting book by price high to low
	public List<BooksDetails>SortByPriceHighToLow(){
		Collections.sort(listOfBook, new SortBook());
		return listOfBook;
	}
	
	// sorting according to best selling
	public List<BooksDetails>sortByselling(){
		Collections.sort(listOfBook, new sortBookcount());
		return listOfBook;
	}
}
	

