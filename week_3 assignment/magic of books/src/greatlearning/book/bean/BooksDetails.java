package greatlearning.book.bean;

public class BooksDetails {
	private String bookName;
	private int NoOfCopies;
	private double price;
	private String GenreOfBooks;
	
	public BooksDetails() {
		super();
		// TODO Auto-generated constructor stub
	}
	public BooksDetails(String bookName, int noOfCopies, double price, String genreOfBooks) {
		super();
		this.bookName = bookName;
		NoOfCopies = noOfCopies;
		this.price = price;
		GenreOfBooks = genreOfBooks;
	}
	public String getBookName() {
		return bookName;
	}
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	public int getNoOfCopies() {
		return NoOfCopies;
	}
	public void setNoOfCopies(int noOfCopies) {
		NoOfCopies = noOfCopies;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getGenreOfBooks() {
		return GenreOfBooks;
	}
	public void setGenreOfBooks(String genreOfBooks) {
		GenreOfBooks = genreOfBooks;
	}
	@Override
	public String toString() {
		return "BooksDetails [bookName=" + bookName + ", NoOfCopies=" + NoOfCopies + ", price=" + price
				+ ", GenreOfBooks=" + GenreOfBooks + "]";
	}
	
	
	
}
