import { Component, OnInit } from '@angular/core';
import { Allbook } from '../allbook';
import { BookserviceService } from '../bookservice.service';

@Component({
  selector: 'app-book-display',
  templateUrl: './book-display.component.html',
  styleUrls: ['./book-display.component.css']
})
export class BookDisplayComponent implements OnInit {
  book:Array<Allbook>=[];
  deleteMsg:string="";
  updateMsg:string="";
  flag:boolean=false
  bookid:number=0
  bname:string=""
  author:string=""
  constructor(public bookdis:BookserviceService) { }

  ngOnInit(): void {
    this.loadBooks();
  }
  loadBooks():void{
    // console.log(":event fired")
     this.bookdis.loadBookDisplay().subscribe(res=>this.book=res );
   }

   deleteProduct(bookid:number){
    console.log(bookid);
    this.bookdis.deleteBookDetails(bookid).subscribe(res=>this.deleteMsg=res,()=>this.loadBooks())
   }
   updateBook(books:Allbook){
      this.flag=true
      this.bookid=books.bookid
      this.bname=books.bname
      this.author=books.author
      
   }
   updateBokName(){
    let books={"uid":this.bookid,"bname":this.bname,"author":this.author}
    console.log(books)
    this.bookdis.updateBookInfo(books).subscribe(res=>this.updateMsg=res,
      ()=>{
      this.loadBooks()
    this.flag=false
   })
  }
}
