import { Injectable } from '@angular/core';
import{HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Allbook } from './allbook';
import { BookDisplayComponent } from './book-display/book-display.component';
@Injectable({
  providedIn: 'root'
})
export class BookserviceService {

  constructor(public http:HttpClient) { }
  // loadBookDetails(){
  //   console.log("service runn")
  //   this.http.get("http://localhost:8181/admin/getallbooks").
  //   subscribe(res=>console.log(res),()=>console.log("done")
  //   );
  loadBookDetails():Observable<Allbook[]>{
    return this.http.get<Allbook[]>("http://localhost:8181/admin/getallbooks")
   }
   loadBookDisplay():Observable<Allbook[]>{
     return this.http.get<Allbook[]>("http://localhost:8181/admin/getallbooks");
     
   }

   deleteBookDetails(bookid:number):Observable<string>{
     return this.http.delete("http://localhost:8181/admin/deletebookById/"+bookid,{responseType:'text'});
   }
   updateBookInfo(Allbook:any):Observable<string>{
    return this.http.patch("http://localhost:8282/user/updateBookname",Allbook,{responseType:'text'})
  }
}
