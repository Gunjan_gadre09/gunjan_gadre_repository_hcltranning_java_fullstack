package com.grtlearning.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import com.grtlearning.bean.FavOrReadLatter;
import com.grtlearning.resource.DbResource;

public class FavOrReadLatterDao {

	public List<FavOrReadLatter> getAllFavlist(){
		List<FavOrReadLatter> listOfFav = new ArrayList<FavOrReadLatter>();
		try {
			Connection con= DbResource.getDbConnection();
			PreparedStatement pstmt= con.prepareStatement("select * from favorreadlatter");
			ResultSet rs=pstmt.executeQuery();
			while(rs.next()) {
				FavOrReadLatter book = new FavOrReadLatter();
				book.setBook_id(rs.getInt(1));
				book.setBook_name(rs.getString(2));
				book.setGeners(rs.getString(3));
				book.setAuthor(rs.getString(4));
				listOfFav.add(book);
				return listOfFav;
			}
		 }catch(Exception e) {
			System.out.println("Store Method Exception "+e);
			
		}
		return null;
		
	  }
	
	public int storeFavBookDao(FavOrReadLatter favbook) {
		try {
			Connection con = DbResource.getDbConnection();
			PreparedStatement pstmt = con.prepareStatement("insert into favorreadlatter values(?,?,?,?,?)");
			pstmt.setInt(1, favbook.getBook_id());
			pstmt.setString(2, favbook.getBook_name());
			pstmt.setString(3, favbook.getGeners());
			pstmt.setString(4, favbook.getAuthor());
			pstmt.setString(5, favbook.getUrl());
			return pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("Store Method Exception "+e);
			return 0;
		}
	}
	
	
}
