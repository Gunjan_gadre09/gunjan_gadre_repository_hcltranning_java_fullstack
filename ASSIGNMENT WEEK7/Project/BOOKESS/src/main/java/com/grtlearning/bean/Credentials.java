package com.grtlearning.bean;

public class Credentials {
	private String email;
	private String name;
	private String passward;
	private int age;
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassward() {
		return passward;
	}
	public void setPassward(String passward) {
		this.passward = passward;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	@Override
	public String toString() {
		return "Credentials [email=" + email + ", name=" + name + ", passward=" + passward + ", age=" + age + "]";
	}
	
}
