package com.grtlearning.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.grtlearning.bean.Credentials;
import com.grtlearning.service.CredentialsService;

/**
 * Servlet implementation class CredentialController
 */
@WebServlet("/CredentialController")
public class CredentialController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CredentialController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		PrintWriter pw = response.getWriter();
		response.setContentType("text/html");
		String email = request.getParameter("user"); 
		String name = request.getParameter("name");
		String passward = request.getParameter("pass"); 
		int age= Integer.parseInt(request.getParameter("age"));
		Credentials cre = new Credentials();
		cre.setEmail(email);
		cre.setName(name);
		cre.setPassward(passward);
		cre.setAge(age);
		CredentialsService es = new CredentialsService();
		int res = es.storeCredentialInfo(cre);
						doGet(request, response);				// it call doGet method and display the records 
		if(res==1) {
			pw.print("SIGNUP SUCCESSFULLY PLEASE LOGIN TO SEE ALL BOOKS");
			RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
			rd.include(request, response);	
		}else {
			pw.print("Didn't created account please SignUp Again");
			RequestDispatcher rd = request.getRequestDispatcher("signup.jsp");
			rd.include(request, response);	
		}
	}

}
