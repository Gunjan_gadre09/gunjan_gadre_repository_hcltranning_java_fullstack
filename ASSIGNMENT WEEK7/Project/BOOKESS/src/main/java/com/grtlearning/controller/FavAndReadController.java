package com.grtlearning.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.grtlearning.bean.Books;
import com.grtlearning.bean.Credentials;
import com.grtlearning.bean.FavOrReadLatter;
import com.grtlearning.service.BooksService;
import com.grtlearning.service.CredentialsService;
import com.grtlearning.service.FavOrReadLatterService;

/**
 * Servlet implementation class FavAndReadController
 */
@WebServlet("/FavAndReadController")
public class FavAndReadController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FavAndReadController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		FavOrReadLatterService es = new FavOrReadLatterService();
		//request.setAttribute("obj1", a);
		List<FavOrReadLatter> listOfbook = es.getAllFav();
		if(listOfbook==null) {
			System.out.println("null");
		}else {
		HttpSession hs = request.getSession();
		hs.setAttribute("obj",listOfbook);
		//hs.setAttribute("obj2", a);
		RequestDispatcher rd = request.getRequestDispatcher("fav.jsp");
		rd.include(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
//		PrintWriter pw = response.getWriter();
//		response.setContentType("text/html");
//		int book_id = Integer.parseInt(request.getParameter("id"));
//		String book_name=request.getParameter("bname");
//		String author =request.getParameter("author");
//		String geners=request.getParameter("geners");
//		String url=request.getParameter("url");
//		FavOrReadLatter fav= new FavOrReadLatter();
//		fav.setBook_id(book_id);
//		fav.setBook_name(book_name);
//		fav.setAuthor(author);
//		fav.setGeners(geners);
//		fav.setUrl(url);
//		FavOrReadLatterService bser = new FavOrReadLatterService();
//		int res = bser.storeFavBookInfo(fav);
//		
//		doGet(request, response);
//		if(res==0) {
//			pw.print("Book ADDED TO FAV LIST");
//			RequestDispatcher rd = request.getRequestDispatcher("fav.jsp");
//			rd.include(request, response);	
//		}else {
//			pw.print("book didin't store");
//			RequestDispatcher rd = request.getRequestDispatcher("fav.jsp");
//			rd.include(request, response);	
//			
//		}
		
		
		
		
		//hs.setAttribute("id", id);
	}

}
