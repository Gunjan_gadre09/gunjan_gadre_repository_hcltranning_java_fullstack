package com.grtlearning.resource;

import java.sql.Connection;
import java.sql.DriverManager;

public class DbResource {
	static Connection con;
	public static Connection getDbConnection() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/week7_gunjan", "root", "gunjansql");
			return  con;
		} catch (Exception e) {
			System.out.println("db Connection method "+e);
		}
		return null;
	}
}
