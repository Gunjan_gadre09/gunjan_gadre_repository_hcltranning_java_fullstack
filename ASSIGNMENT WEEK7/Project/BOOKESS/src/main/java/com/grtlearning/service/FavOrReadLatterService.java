package com.grtlearning.service;

import java.util.List;

import com.grtlearning.bean.FavOrReadLatter;
import com.grtlearning.dao.FavOrReadLatterDao;

public class FavOrReadLatterService {
	
	public List<FavOrReadLatter> getAllFav() {
		FavOrReadLatterDao ed = new FavOrReadLatterDao();
		return ed.getAllFavlist();
	}
	
	public int storeFavBookInfo(FavOrReadLatter book) {
		FavOrReadLatterDao ed = new FavOrReadLatterDao();
		if(ed.storeFavBookDao(book)>0) {
			return 1;
		}else {
			return 0;
		}
	}
	
	
}
