package com.grtlearning.bean;

public class FavOrReadLatter {
	private int book_id;
	private String book_name;
	private String geners;
	private String author;
	private String url;
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int getBook_id() {
		return book_id;
	}
	public void setBook_id(int book_id) {
		this.book_id = book_id;
	}
	public String getBook_name() {
		return book_name;
	}
	public void setBook_name(String book_name) {
		this.book_name = book_name;
	}
	public String getGeners() {
		return geners;
	}
	public void setGeners(String geners) {
		this.geners = geners;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	@Override
	public String toString() {
		return "FavOrReadLatter [book_id=" + book_id + ", book_name=" + book_name + ", geners=" + geners + ", author="
				+ author + ", url=" + url + "]";
	}
	
}
