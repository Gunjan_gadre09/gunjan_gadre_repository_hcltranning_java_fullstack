package com.grtlearning.service;

import java.util.List;

import com.grtlearning.bean.Books;
import com.grtlearning.dao.BooksDao;

public class BooksService {
	public List<Books> getAllBookInfo() {
		BooksDao ed = new BooksDao();
		return ed.getAllBooks();
		//return ed.getAllEmployee().stream().mapToDouble(emp->emp.setSalary(1000)).collect(Collectors.toList());
	}
	public String findBookService(int pid) {
		BooksDao pd  = new BooksDao();
		Books p = pd.findBookDao(pid);
		if(p==null) {
			return "not present";
		}else {
				return p.toString();
		}
}
}
