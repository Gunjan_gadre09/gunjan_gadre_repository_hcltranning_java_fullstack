package com.grtlearning.service;

import java.util.List;

import com.grtlearning.bean.Credentials;
import com.grtlearning.dao.CredentialsDao;

public class CredentialsService {
	public int storeCredentialInfo(Credentials cre) {
		CredentialsDao ed = new CredentialsDao();
		if(ed.storeCredentialsDao(cre)>0) {
			return 1;
		}else {
			return 0;
		}
	}
	public String getCredentialInfo(Credentials cre) {
		CredentialsDao ed = new CredentialsDao();
		Credentials cr= ed.getAllCredentials(cre);
		if(cr==null) {
			return "no record";
		}else {
			return cr.getName();
		}
	}
}
