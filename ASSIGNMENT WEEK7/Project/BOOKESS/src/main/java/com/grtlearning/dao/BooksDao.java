package com.grtlearning.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.grtlearning.bean.Books;
import com.grtlearning.resource.DbResource;

public class BooksDao {
	
	//for getting all books
	public List<Books> getAllBooks(){
		List<Books> listOfbook = new ArrayList<Books>();
		try {
			Connection con= DbResource.getDbConnection();
			PreparedStatement pstmt= con.prepareStatement("select * from books");
			ResultSet rs=pstmt.executeQuery();
			while(rs.next()) {
				Books book = new Books();
				book.setBook_id(rs.getInt(1));
				book.setBook_name(rs.getString(2));
				book.setGeners(rs.getString(3));
				book.setAuthor(rs.getString(4));
				book.setUrl(rs.getString(5));
				listOfbook.add(book);
			}
		 }catch(Exception e) {
			System.out.println("Store Method Exception "+e);
		}
		
		return listOfbook;
	  }
	public Books findBookDao(int book_id) {
		try {
			Connection con = DbResource.getDbConnection();
			PreparedStatement pstmt = con.prepareStatement("select * from books where book_id = ?");
			pstmt.setInt(1, book_id);
			ResultSet rs = pstmt.executeQuery();
			if(rs.next()) {
					Books p = new Books();
					p.setBook_id(rs.getInt(1));
					p.setBook_name(rs.getString(2));
					p.setGeners(rs.getString(3));
					p.setAuthor(rs.getString(4));
					p.setUrl(rs.getString(5));
					return p;
			}
			} catch (Exception e) {
				System.out.println("In findProduct method "+e);
			}
			return null;
	}
}
