package com.grtlearning.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.grtlearning.bean.Books;
import com.grtlearning.bean.Credentials;
import com.grtlearning.service.BooksService;
import com.grtlearning.service.CredentialsService;

/**
 * Servlet implementation class LoginController
 */
@WebServlet("/LoginController")
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    int a=67;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		PrintWriter pw = response.getWriter();
		response.setContentType("text/html");
		String email = request.getParameter("user"); 
		String passward = request.getParameter("pass"); 
		Credentials cre = new Credentials();
		cre.setEmail(email);
		cre.setPassward(passward);
		CredentialsService es = new CredentialsService();
		String res= es.getCredentialInfo(cre);
						doGet(request, response);				// it call doGet method and display the records 
		if(res=="no record") {
			response.setContentType("text/html");
			RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
			pw.print("Didn't login please give valid credentials and login again");
			rd.include(request, response);
		}else {
			
			BooksService es1 = new BooksService();
			request.setAttribute("obj1", res);
			List<Books> listOfbook = es1.getAllBookInfo();
			HttpSession hs = request.getSession();
			hs.setAttribute("obj",listOfbook);
			//hs.setAttribute("obj2", a);
			
			RequestDispatcher rd = request.getRequestDispatcher("successpage.jsp");
			rd.forward(request, response);
			
			
				
		}
	}

}
