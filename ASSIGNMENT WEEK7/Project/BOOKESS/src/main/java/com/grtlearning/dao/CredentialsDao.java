package com.grtlearning.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.grtlearning.bean.Credentials;
import com.grtlearning.resource.DbResource;

public class CredentialsDao {
	public Credentials getAllCredentials(Credentials cre){
	try {
		Connection con= DbResource.getDbConnection();
		PreparedStatement pstmt= con.prepareStatement("select * from credentials where email=? and passward=?");
		pstmt.setString(1, cre.getEmail());
		pstmt.setString(2, cre.getPassward());
		ResultSet rs=pstmt.executeQuery();
		while(rs.next()) {
			Credentials cr = new Credentials();
			cr.setEmail(rs.getString(1));
			cr.setPassward(rs.getString(3));
			cr.setName(rs.getString(2));
			return cr;
		}
	 }catch(Exception e) {
		System.out.println("get Method Exception "+e);
	}
	
	return null;
  }
	
	//for storing information
	
	public int storeCredentialsDao(Credentials cre) {
		try {
			Connection con = DbResource.getDbConnection();
			PreparedStatement pstmt = con.prepareStatement("insert into credentials values(?,?,?,?)");
			pstmt.setString(1, cre.getEmail());
			pstmt.setString(2, cre.getName());
			pstmt.setString(3, cre.getPassward());
			pstmt.setInt(4, cre.getAge());
			return pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("Store Method Exception "+e);
			return 0;
		}
	}
}

	
