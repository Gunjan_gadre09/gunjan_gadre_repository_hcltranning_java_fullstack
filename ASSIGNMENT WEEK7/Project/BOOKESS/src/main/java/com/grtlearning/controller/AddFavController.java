package com.grtlearning.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.grtlearning.bean.FavOrReadLatter;
import com.grtlearning.service.FavOrReadLatterService;

/**
 * Servlet implementation class AddFavController
 */
@WebServlet("/AddFavController")
public class AddFavController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddFavController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		PrintWriter pw = response.getWriter();
		response.setContentType("text/html");
		int book_id = Integer.parseInt(request.getParameter("id"));
		String book_name=request.getParameter("bname");
		String author =request.getParameter("author");
		String geners=request.getParameter("geners");
		String url=request.getParameter("url");
		FavOrReadLatter fav= new FavOrReadLatter();
		fav.setBook_id(book_id);
		fav.setBook_name(book_name);
		fav.setAuthor(author);
		fav.setGeners(geners);
		fav.setUrl(url);
		FavOrReadLatterService bser = new FavOrReadLatterService();
		int res = bser.storeFavBookInfo(fav);
		
		
		doGet(request, response);
		if(res==1) {
			response.setContentType("text/html");
			String msg="Book ADDED TO FAV LIST";
			request.setAttribute("obj10", msg);
			RequestDispatcher rd = request.getRequestDispatcher("favdetails.jsp");
			rd.include(request, response);	
		}else {
			response.setContentType("text/html");
			String msg="book didin't store";
			request.setAttribute("obj10", msg);
			RequestDispatcher rd = request.getRequestDispatcher("favdetails.jsp");
			rd.include(request, response);	
			
		}
		
		
		
		
		//hs.setAttribute("id", id);
	}

	

}
