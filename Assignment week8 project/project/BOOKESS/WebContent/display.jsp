<%@page import="java.util.Iterator"%>
<%@page import="com.grtlearning.bean.Books"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style>
button {
  background-color: #04AA6D;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 20%;
}
body {
align:next;
text-align:center;
font-family: Arial, Helvetica, sans-serif;
}

button:hover {
  opacity: 0.8;
}
span.psw {
  padding-top: 16px;
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
  span.psw {
     display: block;
     float: none;
  }
</style>

</head>
<body>

<span class="psw">CLICK HERE TO MOVE TO LOGIN PAGE <a href="login.jsp"><br><button type="button">LOGIN PAGE</button></a></span>
<hr>

<%
Object obj=request.getAttribute("book");
List<Books> listBook=(List<Books>)obj;
Iterator<Books> li=listBook.iterator();
while(li.hasNext()){
	Books book=li.next();
	%>
	
<div class="row">	

  <h3><div><% out.println(book.getBook_id()); %></div></h3>
  <div>
   <img src="<%=book.getUrl()%>" alt="BOOKIMAGE" width="400" height="500">
  </div>
	
	 <h2><%=book.getBook_name() %></h2>
  <div><h3><%=book.getAuthor() %></h3></div>
  <div><h3><%=book.getGeners() %></h3></div>

</div>

 	<hr>
		
	<%
}
%>
	
</body>
</html>