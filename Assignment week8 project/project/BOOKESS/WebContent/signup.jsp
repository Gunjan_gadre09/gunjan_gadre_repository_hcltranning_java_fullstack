<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>

<style>
body {
text-align:center;
font-family: Arial, Helvetica, sans-serif;}
form {border: 3px solid #f1f1f1;}

input[type=text], input[type=password] {
  width: 50%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  box-sizing: border-box;
}

button {
  background-color: #04AA6D;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 20%;
}

button:hover {
  opacity: 0.8;
}

.cancelbtn {
  width: auto;
  padding: 10px 18px;
  background-color: #f44336;
}

.container {
  padding: 16px;
}

span.psw {
  padding-top: 16px;
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
  span.psw {
     display: block;
     float: none;
  }
  .cancelbtn {
     width: 10%;
  }
}
</style>

</head>
<body>
<h2>SIGN UP FORM</h2>

<form action="credentials.spring" >

  <div class="container">
    <label for="uname"><b> Email</b></label>
    <input type="text" placeholder="Enter Username" name="user" required><br>
    
    <label for="psw"><b> Password</b></label>
    <input type="password" placeholder="Enter Password" name="pass" required><br>
    
    <label for="name"><b> Name</b></label>
    <input type="text" placeholder="Enter Name" name="name" required><br>
    
    <label for="name"><b> Age</b></label>
    <input type="text" placeholder="Enter Your Age" name="age" required><br>
    
    
        
    <button type="submit">SUBMIT</button>
   <button type="button">Reset</button>
    
  </div>
  
 </form>

</body>
</html>