package com.grtlearning.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.grtlearning.bean.Credentials;
import com.grtlearning.bean.ReadLater;

@Repository
public class ReadLaterDao {
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate =jdbcTemplate;
	}

	public int storeFavBook(ReadLater read) {
		return jdbcTemplate.update("insert into readlater values(?,?,?,?,?,?)",read.getBook_id(),read.getBook_name(),read.getAuthor(),read.getGeners(),read.getUrl(),read.getName());
	}
	
	public List<ReadLater> findReadlaterDao(ReadLater cre) {
		List<ReadLater> listOfBook=jdbcTemplate.query("select * from readlater where name=?", new Object[] {cre.getName()},new ReadLaterRawMapper());
		return 	listOfBook;
		}
	
}
class ReadLaterRawMapper implements RowMapper<ReadLater>{

	@Override
	public ReadLater mapRow(ResultSet rs, int rowNum) throws SQLException{
		// TODO Auto-generated method stub
		ReadLater fbook=new ReadLater();
		fbook.setBook_id(rs.getInt(1));
		fbook.setBook_name(rs.getString(2));
		fbook.setAuthor(rs.getString(3));
		fbook.setGeners(rs.getString(4));
		fbook.setUrl(rs.getString(5));
		fbook.setName(rs.getString(6));
		return fbook;
	}
}