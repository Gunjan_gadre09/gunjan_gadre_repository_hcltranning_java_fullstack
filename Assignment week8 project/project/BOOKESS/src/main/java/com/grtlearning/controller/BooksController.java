package com.grtlearning.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.grtlearning.bean.Books;
import com.grtlearning.service.BookService;

@Controller
public class BooksController {
	@Autowired
	BookService bookService;
	@GetMapping(value="display")
	public ModelAndView getAllBooks(HttpServletRequest req) {
		ModelAndView mav= new ModelAndView();
		List<Books> listOfBook= bookService.getBookInfo();
		req.setAttribute("book", listOfBook);
		mav.setViewName("display.jsp");
		return mav;
	}
}
