package com.grtlearning.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.grtlearning.bean.ReadLater;
import com.grtlearning.service.ReadLaterService;

@Controller
@RequestMapping
public class ReadLaterController {
@Autowired
	ReadLaterService readLaterService;
	
	@GetMapping(value="readlaterr")
	public ModelAndView storeFavBook(HttpServletRequest req) {
		int book_id = Integer.parseInt(req.getParameter("id"));
		String book_name = req.getParameter("bname");
		String author=req.getParameter("author");
		String geners=req.getParameter("geners");
		String url=req.getParameter("url");
		String name = req.getParameter("name");
		
		ReadLater rl=new ReadLater();
		rl.setBook_id(book_id);
		rl.setBook_name(book_name);
		rl.setAuthor(author);
		rl.setGeners(geners);
		rl.setUrl(url);
		rl.setName(name);
		
		ModelAndView mav= new ModelAndView();
		int res= readLaterService.storeFavBookInfo(rl);
		
		if(res==1) {
			String name1="ADD TO FAV LIST";
			req.setAttribute("obj", name1);
			mav.setViewName("favdetails.jsp");	
		}else {
			String name1="BOOK NOT ADD TO FAV LIST";
			req.setAttribute("obj", name1);
			mav.setViewName("favdetails.jsp");	
		}
		return mav;
	}
	
	@GetMapping(value="fav")
	public ModelAndView findFavBook(HttpServletRequest req) {
		String name = req.getParameter("name");
		
		ReadLater rl=new ReadLater();
		rl.setName(name);
		ModelAndView mav= new ModelAndView();
		List<ReadLater> res= readLaterService.findFavBookInfo(rl);
		req.setAttribute("msg", res);
		mav.setViewName("fav.jsp");	
		return mav;
	}
}
