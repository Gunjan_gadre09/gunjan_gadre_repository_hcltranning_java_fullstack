package com.grtlearning.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grtlearning.bean.ReadLater;
import com.grtlearning.dao.ReadLaterDao;
@Service
public class ReadLaterService {
	@Autowired
	ReadLaterDao readLaterDao;
	public int storeFavBookInfo(ReadLater read) {
		if(readLaterDao.storeFavBook(read)>0) {
			return 1;
		}else {
			return 0;
		}
	}
	public List<ReadLater> findFavBookInfo(ReadLater rl) {
		// TODO Auto-generated method stub
		return readLaterDao.findReadlaterDao(rl);
	}

}
