package com.grtlearning.controller;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.grtlearning.bean.Books;
import com.grtlearning.bean.Credentials;
import com.grtlearning.service.BookService;
import com.grtlearning.service.CredentialService;

@Controller
@RequestMapping
public class CredentialControler {
	@Autowired
	CredentialService credentialService;
	@Autowired
	BookService bookService;
	
	
	@GetMapping(value="loginn")
	public ModelAndView checkCredential(HttpServletRequest req) {
		String email = req.getParameter("user");
		String passward = req.getParameter("pass");
		Credentials cre = new Credentials();
		cre.setEmail(email);
		cre.setPassward(passward);
		
		ModelAndView mav= new ModelAndView();
		List<Credentials> res= credentialService.CredentialsInfo(cre);
		Iterator<Credentials> li= res.iterator();
		while(li.hasNext()) {
			Credentials cred=li.next();
			if(cre.getEmail()==email &&cre.getPassward()==passward) {
				List<Books> listOfBook= bookService.getBookInfo();
				String msgg="YOU ARE SUCCESSFULLY LOGIN";
				System.out.println(cre.getName());
				req.setAttribute("msg", email);
				req.setAttribute("book", listOfBook);
				mav.setViewName("success.jsp");	
			}else {
				String msgg="YOU CREDENTIALS ARE WRONG PLEASE LOGIN AGAIN TO SEE THE BOOKS";
				req.setAttribute("msg", msgg);
				mav.setViewName("logindetails.jsp");	
			}
		}
		return mav;
	}
	
	
	@GetMapping(value="credentials")
	public ModelAndView storeCredential(HttpServletRequest req) {
		String email = req.getParameter("user");
		String passward = req.getParameter("pass");
		String name = req.getParameter("name");
		int age = Integer.parseInt(req.getParameter("age"));
		
		Credentials cre = new Credentials();
		cre.setEmail(email);
		cre.setName(name);
		cre.setPassward(passward);
		cre.setAge(age);
		ModelAndView mav= new ModelAndView();
		int res= credentialService.storeCredentialsInfo(cre);
		
		if(res==1) {
			String msgg="YOU SIGNIN SUCCESSFULLY PLEASE LOGIN AGAIN TO SEE THE BOOKS";
			req.setAttribute("msg", msgg);
			mav.setViewName("logindetails.jsp");	
		}else {
			mav.setViewName("signup.jsp");	
		}
		return mav;
	}

}
