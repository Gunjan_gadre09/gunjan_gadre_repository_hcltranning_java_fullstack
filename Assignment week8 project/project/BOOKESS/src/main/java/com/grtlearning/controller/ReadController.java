package com.grtlearning.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.grtlearning.bean.Read;
import com.grtlearning.bean.ReadLater;
import com.grtlearning.service.ReadService;

public class ReadController {
	@Autowired
	ReadService readService;
	@GetMapping(value="readbook")
	public ModelAndView storeFavBook(HttpServletRequest req) {
		int book_id = Integer.parseInt(req.getParameter("id"));
		String book_name = req.getParameter("bname");
		String author=req.getParameter("author");
		String geners=req.getParameter("geners");
		String url=req.getParameter("url");
		String name = req.getParameter("name");
		
		Read rl=new Read();
		rl.setBook_id(book_id);
		rl.setBook_name(book_name);
		rl.setAuthor(author);
		rl.setGeners(geners);
		rl.setUrl(url);
		rl.setName(name);
		
		ModelAndView mav= new ModelAndView();
		int res= readService.storeFavBookInfo(rl);
		
		if(res==1) {
			String name1="ADD TO read LIST";
			req.setAttribute("obj", name1);
			mav.setViewName("favdetails.jsp");	
		}else {
			String name1="BOOK NOT ADD TO READ LIST";
			req.setAttribute("obj", name1);
			mav.setViewName("favdetails.jsp");	
		}
		return mav;
	}
	
	@GetMapping(value="showread")
	public ModelAndView findFavBook(HttpServletRequest req) {
		String name = req.getParameter("name");
		
		Read rl=new Read();
		rl.setName(name);
		ModelAndView mav= new ModelAndView();
		List<Read> res= readService.findFavBookInfo(rl);
		req.setAttribute("msg", res);
		mav.setViewName("readlatter.jsp");	
		return mav;
	}

}
