package com.grtlearning.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grtlearning.bean.Read;
import com.grtlearning.bean.ReadLater;
import com.grtlearning.dao.ReadDao;
import com.grtlearning.dao.ReadLaterDao;
@Service
public class ReadService {
	@Autowired
	ReadDao readDao;
	public int storeFavBookInfo(Read read) {
		if(readDao.storeFavBook(read)>0) {
			return 1;
		}else {
			return 0;
		}
	}
	public List<Read> findFavBookInfo(Read rl) {
		// TODO Auto-generated method stub
		return readDao.findReadDao(rl);
	}
}
