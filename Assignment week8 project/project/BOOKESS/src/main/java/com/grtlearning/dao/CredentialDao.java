package com.grtlearning.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.grtlearning.bean.Credentials;

@Repository
public class CredentialDao {
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate =jdbcTemplate;
	}
	
	public List<Credentials> findcredentials(Credentials cre) {
		List<Credentials> listOfBook=jdbcTemplate.query("select * from credentials where email=? and passward =?", new Object[] {cre.getEmail(),cre.getPassward()},new CredentialsRawMapper());
		return 	listOfBook;
		}
	
	public int storecredentials(Credentials cre) {
		//String query="insert into credentials values(?,?,?,?)"cre.getEmail(),cre.getName(),cre.getPassward(),cre.getAge();
		return jdbcTemplate.update("insert into credentials values(?,?,?,?)",cre.getEmail(),cre.getName(),cre.getPassward(),cre.getAge());
	}
}

class CredentialsRawMapper implements RowMapper<Credentials>{

	@Override
	public Credentials mapRow(ResultSet rs, int rowNum) throws SQLException{
		// TODO Auto-generated method stub
		Credentials book=new Credentials();
		book.setEmail(rs.getString(1));
		book.setName(rs.getString(2));
		book.setPassward(rs.getString(3));
		book.setAge(rs.getInt(4));
		return book;
	}
}


