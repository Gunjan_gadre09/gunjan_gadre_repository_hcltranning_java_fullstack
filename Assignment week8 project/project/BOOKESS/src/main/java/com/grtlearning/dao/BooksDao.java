package com.grtlearning.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.grtlearning.bean.Books;

@Repository
public class BooksDao {
@Autowired
JdbcTemplate jdbcTemplate;
	public List<Books> getAllBookDao(){
		return jdbcTemplate.query("select * from books",new BooksRawMapper());
	}

//	public List<Books> findBookById(int book_id) {
//		//jdbcTemplate.query("select * from login where username like ? and password like ?", new Object[] {user,pass},new ProductRowMapper())
//			return jdbcTemplate.query("select * from books where book_id=?",new Object[] {1},new BooksRawMapper());
//	}
}


class BooksRawMapper implements RowMapper<Books>{

@Override
public Books mapRow(ResultSet rs, int rowNum) throws SQLException {
	// TODO Auto-generated method stub
	Books book=new Books();
	book.setBook_id(rs.getInt(1));
	book.setBook_name(rs.getString(2));
	book.setAuthor(rs.getString(3));
	book.setGeners(rs.getString(4));
	book.setUrl(rs.getString(5));
	return book;
}

}

