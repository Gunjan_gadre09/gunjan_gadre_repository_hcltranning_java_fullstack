package com.grtlearning.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grtlearning.bean.Credentials;
import com.grtlearning.dao.CredentialDao;
@Service
public class CredentialService {
@Autowired
CredentialDao credentialDao;
	public List<Credentials> CredentialsInfo(Credentials cre) {
		return credentialDao.findcredentials(cre);
	
	}

	public int storeCredentialsInfo(Credentials cre) {
	if(credentialDao.storecredentials(cre)>0) {
		return 1;
	}else {
		return 0;
	}
  }
	
}
