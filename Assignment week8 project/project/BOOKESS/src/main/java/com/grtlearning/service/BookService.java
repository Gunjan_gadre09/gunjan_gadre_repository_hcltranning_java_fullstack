package com.grtlearning.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grtlearning.bean.Books;
import com.grtlearning.dao.BooksDao;

@Service
public class BookService {
	@Autowired
	BooksDao booksDao;
	public List<Books> getBookInfo(){
		if( booksDao.getAllBookDao()==null) {
			return null;
		}else {
			return booksDao.getAllBookDao();
		}
	}
}
