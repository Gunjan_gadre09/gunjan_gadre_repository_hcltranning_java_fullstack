package com.grtlearning.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.grtlearning.bean.Read;

@Repository
public class ReadDao {
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate =jdbcTemplate;
	}

	public int storeFavBook(Read read) {
		return jdbcTemplate.update("insert into read values(?,?,?,?,?,?)",read.getBook_id(),read.getBook_name(),read.getAuthor(),read.getGeners(),read.getUrl(),read.getName());
	}
	
	public List<Read> findReadDao(Read cre) {
		List<Read> listOfBook=jdbcTemplate.query("select * from read where name=?", new Object[] {cre.getName()},new ReadRawMapper());
		return 	listOfBook;
		}
	
}
class ReadRawMapper implements RowMapper<Read>{

	@Override
	public Read mapRow(ResultSet rs, int rowNum) throws SQLException{
		// TODO Auto-generated method stub
		Read fbook=new Read();
		fbook.setBook_id(rs.getInt(1));
		fbook.setBook_name(rs.getString(2));
		fbook.setAuthor(rs.getString(3));
		fbook.setGeners(rs.getString(4));
		fbook.setUrl(rs.getString(5));
		fbook.setName(rs.getString(6));
		return fbook;
	}
}