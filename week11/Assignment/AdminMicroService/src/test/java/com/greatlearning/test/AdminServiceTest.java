/**
 * 
 */
package com.greatlearning.test;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.greatlearning.bean.Admin;
import com.greatlearning.service.AdminService;

@SpringBootTest
class AdminServiceTest {
	@Autowired
	AdminService adminService;
	
	@Test
	void testRegisterAdmin() {
		Admin emp = new Admin();
		emp.setUsername("Admin2");
		emp.setPassward("12345");
		String res	= adminService.registerAdmin(emp);
		assertEquals(res, "Admin Registered Successfully");
		
	}

	@Test
	void testAdminLoginDetails() {
		//fail("Not yet implemented");
		String ad=adminService.adminLoginDetails("mno@gmail.com", "1234");
		Assertions.assertEquals("Account credentials are not match please try again", ad);
	}

}
