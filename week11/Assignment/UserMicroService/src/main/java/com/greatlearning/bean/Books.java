package com.greatlearning.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Books {
	@Id
	private int bookid;
	private String bname;
	private String author;
	private String geners;
	public int getBookid() {
		return bookid;
	}
	public void setBookid(int bookid) {
		this.bookid = bookid;
	}
	public String getBname() {
		return bname;
	}
	public void setBname(String bname) {
		this.bname = bname;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getGeners() {
		return geners;
	}
	public void setGeners(String geners) {
		this.geners = geners;
	}
	
	@Override
	public String toString() {
		return "Books [bookid=" + bookid + ", bname=" + bname + ", author=" + author + ", geners=" + geners + "]";
	}
}
