package com.greatlearning.controller;

import java.rmi.ServerException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.bean.Books;
import com.greatlearning.bean.Users;
import com.greatlearning.bean.likeBooks;
import com.greatlearning.dao.UserDao;
import com.greatlearning.service.BookService;
import com.greatlearning.service.LikeBookService;
import com.greatlearning.service.UserService;

@RestController
@RequestMapping("user")
public class UserController {
	@Autowired
	UserService userService;
	@Autowired
	BookService bookService;
	@Autowired
	LikeBookService likeBookService;
	
	//for user registration 
		@PostMapping(value = "register",consumes = MediaType.APPLICATION_JSON_VALUE)
		public String storeUserDetails(@RequestBody Users user) {
			return userService.createAccount(user);
		}
	
	//for signin
		@GetMapping(value = "login/{email}/{passward}")
		public String loginUser(@PathVariable("email") String email ,@PathVariable("passward") String passward) {
			return userService.userLoginDetails(email, passward);
		}
	
	//for user signout
		@GetMapping(value="logout")
		public String logoutUser() {
			return "User logout Successfully";
		}
		
		//all books
		@GetMapping(value="getallbook",
				produces = MediaType.APPLICATION_JSON_VALUE)
		public List<Books> getAllBooks(){
			return bookService.getAllBookDetails();
		}
		
		//storing or adding books
		@PostMapping(value = "addbook",consumes = MediaType.ALL_VALUE)
		public String storeNewBooks(@RequestBody Books book) {
			return bookService.addBook(book);
		}
		
		//add to like books
		@PostMapping(value = "addinlike",consumes = MediaType.APPLICATION_JSON_VALUE)
		public String storeInLikeSection(@RequestBody likeBooks likebook) {
			return likeBookService.storeInLike(likebook);
		}
		
		//display all like book
		@GetMapping(value="getalllikebook",
				produces = MediaType.APPLICATION_JSON_VALUE)
		public List<likeBooks> getAlllikeBook(){
			return likeBookService.getAllLikeBook();
		}
		
		//displaying all object user
		@GetMapping(value="getuser",
				produces = MediaType.APPLICATION_JSON_VALUE)
		public List<Users> getAllUser(){
			return userService.getAllUserDetails();
		}
		//
		@GetMapping(value = "likebookbyname/{uname}")
		public List<likeBooks> getLikeBookByName(@PathVariable("uname") String uname) {
			return likeBookService.likeBookName(uname);
		}
		
		//delete user details based upon id
		@DeleteMapping(value = "deleteuser/{uid}")
		public String deleteUserInfo(@PathVariable("uid") int uid) {
			return userService.deleteUser(uid);
		}
		
		//delete book using bookid
		@DeleteMapping(value = "deletebook/{bookid}")
		public String deletebookInfo(@PathVariable("bookid") int bookid) {
			return bookService.deleteBook(bookid);
		}
		
		@PatchMapping(value = "updateBookname")
		public String updateBookDetails(@RequestBody Books book) {
			return bookService.updateBookById(book);
		}
		
		@PatchMapping(value = "updateuser")
		public String updateUserDetails(@RequestBody Users uu) {
			return userService.updateUserInfo(uu);
		}
		
	
		@PostMapping(value = "Store" ,consumes = MediaType.APPLICATION_JSON_VALUE)
		public String storesUserDetails(@RequestBody Users user)  {
			String uu=userService.createAccount(user);
			System.out.println(user);
			return uu;
			
		
		}
}
