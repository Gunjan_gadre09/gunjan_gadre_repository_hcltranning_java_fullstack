package com.greatlearning.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.bean.likeBooks;
import com.greatlearning.dao.LikeBookDao;

@Service
public class LikeBookService {
	@Autowired
	LikeBookDao likeBookDao;
	public String storeInLike(likeBooks likebook) {
		if(likeBookDao.existsById(likebook.getBookid())) {
			return "This likebook is already present";
		}else {
			likeBookDao.save(likebook);
			return "likebook Details Stored Successfully stored successfully";
		}
	}

	public List<likeBooks> getAllLikeBook() {
		
		return likeBookDao.findAll();
	}

	public List<likeBooks> likeBookName(String uname) {
		List<likeBooks> book= likeBookDao.getByName(uname);
		if(book==null) {
			return null;
		}else {
		return book;
		}
	}
}
