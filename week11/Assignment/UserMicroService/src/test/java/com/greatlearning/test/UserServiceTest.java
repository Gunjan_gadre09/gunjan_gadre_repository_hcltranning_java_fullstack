package com.greatlearning.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.greatlearning.bean.Users;
import com.greatlearning.service.UserService;
@SpringBootTest
class UserServiceTest {
	@Autowired
	UserService userService;
	
	@Test
	void testUserLoginDetails() {
		String emp= userService.userLoginDetails("abcc@gmail.com", "1234");
		Assertions.assertEquals("Account credentials are not match please try again", emp);
		
	}

	@Test
	void testCreateAccount() {
		Users emp = new Users();
		emp.setUid(105);
		emp.setName("Ravi");
		emp.setEmail("ravi@gmail.com");
		emp.setPassward("0987h");
		String res	= userService.createAccount(emp);
		assertEquals(res, "This user is already present");
	
	}


	@Test
	void testDeleteUser() {
		String emp= userService.deleteUser(22);
		Assertions.assertEquals("user details not present for this user", emp);
		
	}

//	@Test
//	void testUpdateUserInfo() {
//		Users emp = new Users();
//		emp.setUid(105);
//		emp.setEmail("ravi@gmail.com");
//		emp.setName("Ranii");
//		String res	= userService.updateUserInfo(emp);
//		assertEquals(res, "user details not present for this user");
//	
//	}

}
