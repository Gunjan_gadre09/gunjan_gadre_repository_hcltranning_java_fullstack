package com.greatlearning.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.web.client.RestTemplate;

import com.greatlearning.bean.Books;
import com.greatlearning.bean.Users;
import com.greatlearning.bean.likeBooks;

@SpringBootTest
class UserControllerTest {
	String baseUrl = "http://localhost:8282/user";
	@Test
	void testStoreUserDetails() {
		RestTemplate restTemplate = new RestTemplate();
		Users emp = new Users();
		emp.setUid(106);
		emp.setName("Raju");
		emp.setEmail("Raju@gmail.com");
		emp.setPassward("raju99");
		String res	= restTemplate.postForObject(baseUrl+"/register", emp, String.class);
		assertEquals(res, "This user is already present");
	
	}

	@Test
	void testLoginUser() {
		RestTemplate restTemplate = new RestTemplate();
		String res	= restTemplate.getForObject(baseUrl+"/login/Raju@gmail.com/raju99", String.class);
		assertEquals(res, "Login Successfully");
	
	}

	@Test
	void testLogoutUser() {
		RestTemplate restTemplate = new RestTemplate();
		String res	= restTemplate.getForObject(baseUrl+"/logout", String.class);
		assertEquals(res, "User logout Successfully");
	}

	@Test
	void testStoreNewBooks() {
		RestTemplate restTemplate = new RestTemplate();
		Books emp = new Books();
		emp.setBookid(106);
		emp.setBname("Wings of fire");
		emp.setAuthor("Abdul kalam");
		emp.setGeners("Autobio graphy");
		String res	= restTemplate.postForObject(baseUrl+"/addbook", emp, String.class);
		assertEquals(res, "This book is already present");
	
	}

	@Test
	void testStoreInLikeSection() {
		RestTemplate restTemplate = new RestTemplate();
		likeBooks emp = new likeBooks();
		emp.setBookid(106);
		emp.setBname("Wings of fire");
		emp.setAuthor("Abdul kalam");
		emp.setGeners("Autobio graphy");
		emp.setUname("avika");
		String res	= restTemplate.postForObject(baseUrl+"/addinlike", emp, String.class);
		assertEquals(res, "This likebook is already present");
	
	}


	@Test
	void testDeleteUserInfo() {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.delete(baseUrl+"/deleteuser/3");
	}

	@Test
	void testDeletebookInfo() {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.delete(baseUrl+"/deletebook/3");
	
	}

}
