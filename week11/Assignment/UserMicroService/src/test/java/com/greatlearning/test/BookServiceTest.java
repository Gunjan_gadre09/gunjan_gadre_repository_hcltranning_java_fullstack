package com.greatlearning.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.greatlearning.bean.Books;
import com.greatlearning.bean.Users;
import com.greatlearning.service.BookService;
@SpringBootTest
class BookServiceTest {
@Autowired
BookService bookService;

	
	@Test
	void testAddBook() {
		Books emp = new Books();
		emp.setBookid(21);
		emp.setBname("abcc");
		emp.setAuthor("ravinath");
		emp.setGeners("Drama");
		String book	= bookService.addBook(emp);
		assertEquals(book, "This book is already present");
	
	}

	@Test
	void testDeleteBook() {
		String emp= bookService.deleteBook(22);
		Assertions.assertEquals("book details not present for this book id", emp);
		
	}

	
}
