package com.greatlearning.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.greatlearning.bean.likeBooks;
import com.greatlearning.service.LikeBookService;
@SpringBootTest
class LikeBookServiceTest {
@Autowired
LikeBookService likeBookService;

	@Test
	void testStoreInLike() {
		likeBooks emp = new likeBooks();
		emp.setBookid(10);
		emp.setBname("catch22");
		emp.setAuthor("kkkk");
		emp.setGeners("autobiography");
		emp.setUname("Ashii");
		String res	= likeBookService.storeInLike(emp);
		assertEquals(res, "likebook Details Stored Successfully stored successfully");
	
	}
}
