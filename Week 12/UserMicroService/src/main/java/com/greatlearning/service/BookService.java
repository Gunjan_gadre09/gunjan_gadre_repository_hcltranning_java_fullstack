package com.greatlearning.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.bean.Books;
import com.greatlearning.dao.BookDao;

@Service
public class BookService {
	@Autowired
	BookDao booksDao;
	public List<Books> getAllBookDetails() {
		return booksDao.findAll();
	}
	
	public String addBook(Books book) {
		if(booksDao.existsById(book.getBookid())) {
			return "This book is already present";
		}else {
			booksDao.save(book);
			return "books Details Stored Successfully stored successfully";
		}
	}
	
	public String deleteBook(int bookid) {
		if(!booksDao.existsById(bookid)) {
			return "book details not present for this book id";
			}else {
			booksDao.deleteById(bookid);
			return "Book details deleted successfully";
			}	
	}

	public String updateBookById(Books book) {
		if(!booksDao.existsById(book.getBookid()) ){
			return "books details not present for this id";
			}else {
			Books u= booksDao.getById(book.getBookid()) ;
			u.setBname(book.getBname());						
			booksDao.saveAndFlush(u);				
			return "book updated successfully";
	}

	}
}
