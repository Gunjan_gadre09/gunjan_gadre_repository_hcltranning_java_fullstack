import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminloginComponent } from './adminlogin/adminlogin.component';
import { AdminsignupComponent } from './adminsignup/adminsignup.component';
import { BookDisplayComponent } from './book-display/book-display.component';
import { BooksComponent } from './books/books.component';
import { UserDisplayComponent } from './user-display/user-display.component';
import { UserloginComponent } from './userlogin/userlogin.component';
import { UsersignupComponent } from './usersignup/usersignup.component';

const routes: Routes = [
  {path:"userlogin",component:UserloginComponent},
  {path:"usersignup",component:UsersignupComponent},
  {path:"adminlogin",component:AdminloginComponent},
  {path:"adminsignup",component:AdminsignupComponent},
  {path:"books",component:BooksComponent},
  {path:"user-display",component:UserDisplayComponent},
  {path:"book-display",component:BookDisplayComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
