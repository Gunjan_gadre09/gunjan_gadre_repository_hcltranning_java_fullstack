import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Userservice } from './userservice';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(public http:HttpClient) { }
  storeUserDetails(user:UsersService):Observable<string>{
    return this.http.post("http://localhost:8282/user/register",user,{responseType:'text'})
  }
  userLoginDetails(email:any,passward:string):Observable<string>{
    return this.http.get<string>("http://localhost:8282/user/login/"+email/+passward)
 
  }
  loadUserDisplay():Observable<Userservice[]>{
    return this.http.get<Userservice[]>("http://localhost:8181/admin/getallusers");
    
  }
  deleteUsersInfo(uid:number):Observable<string>{
    return this.http.delete("http://localhost:8181/admin/deleteuser/"+uid,{responseType:'text'})
  }
  updateUsersInfo(user:any):Observable<string>{
    return this.http.patch("http://localhost:8282/user/updateuser",user,{responseType:'text'})
  }
}
