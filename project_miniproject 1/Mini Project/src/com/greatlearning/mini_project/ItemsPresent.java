package com.greatlearning.mini_project;

import java.time.LocalDateTime;

public class ItemsPresent {
	private String username;
	private String item;
	private double total;
	private LocalDateTime date;
	//private String email;
	//private String passward;
	
	public String getUsername() {
		return username;
	}
	
	public ItemsPresent() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ItemsPresent(String username, String item, double total, LocalDateTime date) {
		super();
		this.username = username;
		this.item = item;
		this.total = total;
		this.date = date;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public LocalDateTime getDate() {
		return date;
	}
	public void setDate(LocalDateTime date) {
		this.date = date;
	}
	@Override
	public String toString() {
		return "ItemsPresent [username=" + username + ", item=" + item + ", total=" + total + ", date=" + date + "]";
	}
	
}