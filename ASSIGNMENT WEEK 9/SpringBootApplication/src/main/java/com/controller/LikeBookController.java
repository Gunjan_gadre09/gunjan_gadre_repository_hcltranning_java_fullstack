package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.bean.LikeBook;
import com.service.LikeService;

@RestController
@RequestMapping("likebook")
public class LikeBookController {
	@Autowired
	LikeService likeService;
	
	@PostMapping(value="storelikebook",
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeBookInfo(@RequestBody LikeBook book) {
		return likeService.StoreBookDetails(book);
	}
	
	@GetMapping(value="getlikebook",
		produces = MediaType.APPLICATION_JSON_VALUE)
	public List<LikeBook> getBooksDetails(){
		return likeService.getAllBook();
	}
	
	@DeleteMapping(value="deletelikebook/{bookid}")
	public String detelebook(@PathVariable("bookid") int bookid) {
		return likeService.deleteBookById(bookid);
	}
}
