package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bean.Book;
import com.service.BookService;

@RestController
@RequestMapping("book")
public class BookController {
	@Autowired
	BookService bookService;
	
	@PostMapping(value="storebook",
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeBookInfo(@RequestBody Book book) {
		return bookService.StoreBookDetails(book);
	}
	
	@GetMapping(value="getbook",
		produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Book> getBooksDetails(){
		return bookService.getAllBook();
	}
	
	@DeleteMapping(value="deletebook/{bookid}")
	public String detelebook(@PathVariable("bookid") int bookid) {
		return bookService.deleteBookById(bookid);
	}
	
	@PatchMapping(value="updatebook")
	public String Updatebook(@RequestBody Book book) {
		return bookService.updateBookDetails(book);
	}
}
