package com.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Entity
@ToString
@RequiredArgsConstructor
public class User {
	@Id
private String email;
private String passward;
private String name;
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getPassward() {
	return passward;
}
public void setPassward(String passward) {
	this.passward = passward;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}

}
