package com.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.RequiredArgsConstructor;
import lombok.ToString;
@Entity
@RequiredArgsConstructor
@ToString
public class Book {
	@Id
private int bookid;
private String bname;
private String geners;
private String author;
private int price;
public int getBookid() {
	return bookid;
}
public void setBookid(int bookid) {
	this.bookid = bookid;
}
public String getBname() {
	return bname;
}
public void setBname(String bname) {
	this.bname = bname;
}
public String getGeners() {
	return geners;
}
public void setGeners(String geners) {
	this.geners = geners;
}
public String getAuthor() {
	return author;
}
public void setAuthor(String author) {
	this.author = author;
}
public int getPrice() {
	return price;
}
public void setPrice(int price) {
	this.price = price;
}

}
