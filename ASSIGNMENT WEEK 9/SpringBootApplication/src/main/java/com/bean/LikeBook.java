package com.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class LikeBook {
	@Id
	private int bookid;
	private String bname;
	private String name;
	private String author;
	private int price;
	public int getBookid() {
		return bookid;
	}
	public void setBookid(int bookid) {
		this.bookid = bookid;
	}
	public String getBname() {
		return bname;
	}
	public void setBname(String bname) {
		this.bname = bname;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	@Override
	public String toString() {
		return "LikeBook [bookid=" + bookid + ", bname=" + bname + ", name=" + name + ", author=" + author
				+ ", price=" + price + "]";
	}
	
}
